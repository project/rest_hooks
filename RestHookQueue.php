<?php

/**
 * Queue to send rest hooks.
 */
class RestHookQueue extends SystemQueue {

  public function __construct() {
    parent::__construct('rest_hooks_queue');
  }

  public static function processItem($data) {
    _rest_hooks_process($data['hook_id'], $data['entity_id']);
  }

}
